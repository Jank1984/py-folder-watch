import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
import watchdog

class Changer(watchdog.events.FileSystemEventHandler):

    def dispatch(self, event):
        # print(event)
        if(event.event_type == "modified"):
            self.copyFolder()

    def copyFolder():
        print("copyFolder")





if __name__ == "__main__":
    # logging.basicConfig(level=logging.INFO,
    #                     format='%(asctime)s - %(message)s',
    #                     datefmt='%Y-%m-%d %H:%M:%S')
    if len(sys.argv) < 2: exit()
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    # event_handler = LoggingEventHandler()
    observer = Observer()
    changer = Changer()
    # observer.schedule(event_handler, path, recursive=True)
    observer.schedule(changer, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
