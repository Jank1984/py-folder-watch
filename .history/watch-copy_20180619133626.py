import sys
import time
import os
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import watchdog
import distutils.core

class Changer(FileSystemEventHandler):

    ignore_list = []
    src = None
    dest = None

    def __init__(self, src, dest, ignore_list = []):
        self.src = src
        self.dest = dest
        self.ignore_list = ignore_list
        print("ignore_list", ignore_list)
#
    def dispatch(self, event):
        print(event)
        if(event.event_type == "modified"):
            # if(ignore_list)
            # any([s for s in self.ignore_list if "nope" in s])
            # src_path = event.src_path
            for s in self.ignore_list:
                if s in event.src_path: return
            self.copyFolder()

    def copyFolder(self):
        distutils.dir_util.copy_tree(self.src, self.dest)





if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("args: src dest [ignore1;ignore2;...]")
        exit()

    path = sys.argv[1]
    dest_path = sys.argv[2]
    ignore_list = sys.argv[3].split(";") if len(sys.argv) > 2 else []
    print("watching '{}'...".format(path))
    observer = Observer()
    changer = Changer(os.path.realpath(path), os.path.realpath(dest_path), ignore_list)
    observer.schedule(changer, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(5)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
