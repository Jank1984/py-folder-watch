import sys
import time
import os
# import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import watchdog
# import shutil#
import distutils.core

class Changer(FileSystemEventHandler):

    ignore_list = []
    src = None
    dest = None

    def __init__(self, src, dest, ignore_list = []):
        self.src = src
        self.dest = dest
        self.ignore_list = ignore_list
        print("ignore_list", ignore_list)

    def dispatch(self, event):
        print(event)
        if(event.event_type == "modified"):
            if(ignore_list)
            self.copyFolder()

    def copyFolder(self):
        # print("copyFolder")
        # shutil.copy(self.src, self.dest)
        distutils.dir_util.copy_tree(self.src, self.dest)





if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("args: src dest [ignore1;ignore2;...]")
        exit()
    path = sys.argv[1]
    dest_path = sys.argv[2]
    ignore_list = sys.argv[3].split(";") if len(sys.argv) > 2 else []
    print("watching '{}'...".format(path))
    observer = Observer()
    changer = Changer(os.path.realpath(path), os.path.realpath(dest_path), ignore_list)
    # observer.schedule(event_handler, path, recursive=True)
    observer.schedule(changer, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
