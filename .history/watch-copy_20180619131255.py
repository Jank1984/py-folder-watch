import sys
import time
# import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import watchdog
import shutil

class Changer(FileSystemEventHandler):

    src = ""
    dest = ""

    def __init__(self, src, dest):#
        self.src = src
        self.dest = dest

    def dispatch(self, event):
        print(event)
        if(event.event_type == "modified"):
            self.copyFolder()

    def copyFolder(self):
        # print("copyFolder")
        shutil.copy(self.src, self.dest)





if __name__ == "__main__":
    # logging.basicConfig(level=logging.INFO,
    #                     format='%(asctime)s - %(message)s',
    #                     datefmt='%Y-%m-%d %H:%M:%S')
    if len(sys.argv) < 2:
        print("args: src dest")
        exit()
    path = sys.argv[1]
    destpath = sys.argv[2]
    # event_handler = LoggingEventHandler()
    observer = Observer()
    changer = Changer(path, destpath)
    # observer.schedule(event_handler, path, recursive=True)
    observer.schedule(changer, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
