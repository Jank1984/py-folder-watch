__author__ = "kostjan"
__version__ = "1.3"


import sys
import time
import os
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import watchdog
# import distutils.core
import shutil

# from datetime import datetime, timedelta
# from functools import wraps
# #https://gist.github.com/ChrisTM/5834503
# class throttle(object):
#     """
#     Decorator that prevents a function from being called more than once every
#     time period.
#     To create a function that cannot be called more than once a minute:
#         @throttle(minutes=1)
#         def my_fun():
#             pass
#     """
#     def __init__(self, seconds=0, minutes=0, hours=0):
#         self.throttle_period = timedelta(
#             seconds=seconds, minutes=minutes, hours=hours
#         )
#         self.time_of_last_call = datetime.min

#     def __call__(self, fn):
#         @wraps(fn)
#         def wrapper(*args, **kwargs):
#             now = datetime.now()
#             time_since_last_call = now - self.time_of_last_call

#             if time_since_last_call > self.throttle_period:
#                 self.time_of_last_call = now
#                 return fn(*args, **kwargs)

#         return wrapper



# from threading import Timer


# def debounce(wait):
#     """ Decorator that will postpone a functions
#         execution until after wait seconds
#         have elapsed since the last time it was invoked. """
#     def decorator(fn):
#         def debounced(*args, **kwargs):
#             def call_it():
#                 fn(*args, **kwargs)
#             try:
#                 debounced.t.cancel()
#             except(AttributeError):
#                 pass
#             debounced.t = Timer(wait, call_it)
#             debounced.t.start()
#         return debounced
#     return decorator



class Changer(FileSystemEventHandler):
    
    ignore_list = []
    src = None
    dest = None

    def __init__(self, src, dest, ignore_list = []):
        self.src = src
        self.dest = dest
        self.ignore_list = ignore_list

    # @throttle(seconds=10)
    # @debounce(1)
    def dispatch(self, event):
        # print(event)
        if(event.event_type == "modified" or event.event_type == "created"):
            for s in self.ignore_list:
                if s in event.src_path: return
            # print(event)
            self.copyFolder()

    def copyFolder(self):


        file_length = None
        for root, dirs, files in os.walk(self.src, topdown = False):
            file_length = len(files)
            for name in files:

                skip = False
                for ig in self.ignore_list:
                    if name in ig: skip = True
                    if ig in root: skip = True
                if skip: continue

                src_file_path = os.path.join(os.path.normpath(root), os.path.normpath(name))
                dest_file_path = os.path.join(os.path.normpath(self.dest), os.path.normpath(root))
                dest_file_file = os.path.join(dest_file_path, os.path.normpath(name))

                if not os.path.isdir(dest_file_path):
                    os.makedirs(dest_file_path)
                try:
                    if not os.path.isfile(src_file_path): continue
                    shutil.copyfile(src_file_path, dest_file_file)
                except Exception as err:
                    print(err)
                    continue
        print("Copied {} file(s).".format(file_length))
        # return "Copied {} file(s).".format(file_length)


            





if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("args: src dest [ignore1;ignore2;...]")
        exit()

    path = sys.argv[1]
    dest_path = sys.argv[2]
    ignore_list = sys.argv[3].split(";") if len(sys.argv) > 3 else []
    print("watch-copy {}".format(__version__))
    print("watching '{}'...".format(os.path.realpath(path)))
    observer = Observer()
    # changer = Changer(os.path.realpath(path), os.path.realpath(dest_path), ignore_list)
    changer = Changer(path, dest_path, ignore_list)
    observer.schedule(changer, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(5)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
